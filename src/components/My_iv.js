import React, { Component } from 'react';
import { View, Image } from 'react-native';

class ImageComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <View>
        <Image source={require('../components/Assets/iv.jpg')} />
      </View>
    );
  }
}

export default ImageComponent;