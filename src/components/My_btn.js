import React, { Component } from 'react';
import { View, Button } from 'react-native';
import axios from 'axios';

class ButtonComponent extends Component {
  constructor(props) {
    super(props);
    this.state = { title: 'Activate' };
  }

  async onClick() {
    try {
      let self = this;
      self.setState({
        title: 'Waiting'
      });
      let res = await axios.post(this.props.url, { ...this.props.body });
      if (res.ok) {
        self.setState({
          title: 'Activated'
        });
      }
    } catch (error) {
      console.log(error);
    }
  }

  render() {
    return (
      <View>
        <Button title={this.state.title} onPress={this.onClick.bind(this)} />
      </View>
    )
  }
}

export default ButtonComponent;