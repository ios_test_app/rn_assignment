/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */


import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
} from 'react-native';

import {
  Header,
  LearnMoreLinks,
  Colors,
  DebugInstructions,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';
import ImageComponent from './src/components/My_iv';
import ButtonComponent from './src/components/My_btn';


const App: () => React$Node = () => {
  return (
    <View style={styles.container}>
      <ImageComponent imageUrl='../' />
      <Text>Open up App.js to start working on your app!</Text>
      <ButtonComponent url='http://rakbank-test.mocklab.io/activation' body={{
        productId: '82jqp008d2l00',
        emirate: 'Abu Dhabi'
      }} />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center'
  }
});

export default App;
